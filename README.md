# Pick_color_rgb

This is a code to recognize and inform the colors RGB, percentage RGB and hexadecimal, from the left mouse button click on the color you want to discover. It runs directly from the terminal and works over any image or program on your monitor. To stop the program, just click with the right mouse button.

Este é um código para reconhecer e informar as cores RGB, RGB percentual e hexadecimal, a partir do clique do botão esquerdo do mouse em cima da cor que se quer descobrir. Ele roda diretamente do terminal e funciona sobre qualquer imagem ou programa no seu monitor. Para interromper o programa, é só clicar com o botão direito do mouse.